"use strict";

const Package = require("./package.json");
const Path = require("path");
const Dotenv = require("dotenv-webpack");
const {VueLoaderPlugin} = require("vue-loader");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    mode: process.env.NODE_ENV,
    devtool: "cheap-source-map",
    entry: Package.main,
    output: {
        path: Path.resolve(
            __dirname,
            Package.environment[process.env.NODE_ENV].bundle
        ),
        filename: "[name].js"
    },
    watch: process.env.NODE_ENV === "development",
    watchOptions: {
        ignored: /node_modules/
    },
    optimization: {
        runtimeChunk: "single",
        splitChunks: {
            chunks: "all",
            // minSize: 30000,
            // maxSize: 50000,
            // minChunks: 1,
            // maxAsyncRequests: 6,
        }
    },
    plugins: [
        new Dotenv({
            path: process.env.NODE_ENV === "production" ?
                "./.env.production" :
                "./.env.development",
            defaults: true,
            silent: false,
        }),
        new VueLoaderPlugin(),
        new HtmlWebpackPlugin({
            hash: true,
            filename: "./index.html" // `${Package.environment[process.env.NODE_ENV].bundle}index.html`
        })
    ],
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: ["babel-loader"]
            },
            {
                test: /\.css$/,
                use: [
                    "style-loader",
                    "css-loader"
                ]
            },
            {
                test: /\.vue$/,
                loader: "vue-loader",
                include: Path.resolve("./", "./src/")
            },
            {
                test: /\.(png|svg|jpg|gif|jpeg)$/,
                use: [{
                    loader: "file-loader",
                    options: {
                        name: (() => process.env.BUNDLE_SOURCE_MAPS === "true" ?
                            "[path][name].[ext]" :
                            "[contenthash].[ext]"
                        )(),
                        outputPath: "style/"
                    }
                }]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [{
                    loader: "file-loader",
                    options: {
                        name: "[name].[ext]",
                        outputPath: "fonts/"
                    }
                }]
            }
        ]
    }
};

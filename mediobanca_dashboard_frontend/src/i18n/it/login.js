export default {
    title: "Effettua il login",
    username: {
        label: "Username",
        placeholder: "Es: mario.rossi@mediobanca.it"
    },
    password: {
        label: "Password",
        placeholder: ""
    },
    login: {
        label: "Login"
    },
    notifications: {
        loginFail: {
            title: "Login fallito",
            content: "Non hai inserito delle credenziali corrette",
            name: "loginFail"
        }
    }
};

"use strict";

export default {
    dashboard: "Sintesi",
    consistencyChecks: "Consistency checks",
    trends: "Trend",
    finrepVsCorep: "Finrep vs Corep",
};

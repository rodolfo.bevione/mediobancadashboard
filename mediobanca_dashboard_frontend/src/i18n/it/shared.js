/* eslint-disable max-len */
"use strict";

export default {
    error401: {
        title: "Login richiesto",
        content: "Sessione scaduta. Rieffettua il login per continuare"
    },
    error403: {
        title: "Operazione fallita",
        content: "Impossibile accedere alla risorsa richiesta; non hai sufficienti privilegi. Contatta l'amministratore per riceverli."
    },
    error404: {
        title: "Operazione fallita",
        content: "La risorsa richiesta non è stata trovata."
    },
    error500: {
        title: "Operazione fallita",
        content: "L'operazione di {operationName} è fallita a causa di problemi tecnici. Il team IT è al lavoro per risolvere il problema"
    },
    app: {
        title: "R.R."
    }
};

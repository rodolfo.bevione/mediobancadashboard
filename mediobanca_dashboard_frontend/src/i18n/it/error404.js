"use strict";

export default {
    title: "Pagina non trovata",
    header: "Ooooooops",
    content: "La pagina che cercavi non è disponibile.",
    backToSafety: "Clicca qui per tornare alla pagina principale"
};

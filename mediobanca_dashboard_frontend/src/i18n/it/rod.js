/* eslint-disable require-jsdoc */
/* eslint-disable new-cap*/
"use strict";
const Axios = require("axios");

async function run(rodBot) {
    const ResponseClass = rodBot.Utils().chat.messages.WC;
    var url = "https://en.wikipedia.org/w/api.php?"
    var params = {
        "action": "query",
        "format": "json",
        "list": "random",
        "rnlimit": "1"
    };
    var index = 0;
    Reflect.ownKeys(params).forEach((element) => {
        url = url + element + "=" + params[element];
        url = url + (index !== 3 ?
            "&" :
            "");
        index++;
    });
    var result = await Axios.get(url);
    var response = (new ResponseClass()).
        addResponse().
            Timeout(1000).
            Text("Ho preso una pagina a caso da Wikipedia").
            Parent().
        addResponse().
            Timeout(1000).
            Text(`Oggi si parla di ${result.data.query.random[0].title}`).
            Parent().
        Category("Random");
    rodBot.Answer(response);
    rodBot.Deliver();
}

module.exports = run;

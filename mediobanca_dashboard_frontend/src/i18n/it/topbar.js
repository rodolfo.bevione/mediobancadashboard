"use strict";

export default {
    hamburger: {
        collapse: "Nascondi",
        expande: "Espandi"
    },
    search: {
        label: "Cerca",
        placeholder: 'Es: "Dashboard"',
        tooltip: "Effettua una ricerca all'interno del sito",
        results: {
            files: "File",
            routes: "Pagine",
            users: "Utenti"
        }
    },
    profileMenu: {
        label: {
            logged: "Ciao {name}",
            logging: "Login richiesto"
        },
        profile: "Profilo",
        notifications: "Notifiche",
        logout: "Slogga",
        disconnect: "Disconnetti tutti i dispositivi"
    },
    fileUpload: "Carica file"
};

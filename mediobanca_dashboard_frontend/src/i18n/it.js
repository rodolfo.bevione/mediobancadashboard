"use strict";

import Login from "./it/login";
import Shared from "./it/shared";
import Sidebar from "./it/sidebar";
import Topbar from "./it/topbar";
import Error404 from "./it/error404";
import Home from "./it/home";
import CreditCheck from "./it/creditCheck";
import FinrepVsCorep from "./it/finrepVsCorep";
import Trend from "./it/trend";
import Profile from "./it/profile";

export default {
    login: Login,
    shared: Shared,
    sidebar: Sidebar,
    topbar: Topbar,
    error404: Error404,
    home: Home,
    creditCheck: CreditCheck,
    finrepVsCorep: FinrepVsCorep,
    trend: Trend,
    profile: Profile
};

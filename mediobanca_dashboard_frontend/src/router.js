"use strict";

import Vue from "vue";
import VueRouter from "vue-router";
import Home from "./template/view/home.vue";
import CreditCheck from "./template/view/creditCheck.vue";
import Trend from "./template/view/trend.vue";
import Error404 from "./template/view/error404.vue";

Vue.use(VueRouter);

const Router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes: [
        {
            // In routes we store both "title" -computed at runtime- and "pathTitle" -where, in i18n, our page title is
            // That's because "searching" allows us to search for pages too
            path: "/",
            name: "home",
            components: {
                content: Home
            },
            meta: {
                pathTitle: "home.title"
            }
        },
        {
            path: "/credit-check",
            name: "creditCheck",
            components: {
                content: CreditCheck
            },
            meta: {
                pathTitle: "creditCheck.title"
            }
        },
        {
            path: "/finrep-vs-corep",
            name: "finrepVsCorep",
            components: {
                content: Home
            },
            meta: {
                pathTitle: "finrepVsCorep.title"
            }
        },
        {
            path: "/trend",
            name: "trend",
            components: {
                content: Trend
            },
            meta: {
                pathTitle: "trend.title"
            }
        },
        {
            path: "/profile",
            name: "profile",
            components: {
                content: Home
            },
            meta: {
                pathTitle: "profile.title"
            }
        },
        {
            path: "*",
            components: {
                content: Error404
            },
            meta: {
                pathTitle: "error404.title"
            }
        }
    ]
});

export {Router};

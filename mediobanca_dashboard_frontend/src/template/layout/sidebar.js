"use strict";

/**
 * @example {
 *     icon: "icon name",
 *     name: "path to the i18n translation of this element"
 * }
 */
export default [
    {
        icon: "el-icon-menu",
        name: "sidebar.dashboard",
        route: {
            name: "home"
        }
    },
    {
        icon: "el-icon-search",
        name: "sidebar.consistencyChecks",
        route: {
            name: "creditCheck"
        }
    },
    {
        icon: "el-icon-data-analysis",
        name: "sidebar.trends",
        route: {
            name: "trend"
        }
    },
    {
        icon: "el-icon-stopwatch",
        name: "sidebar.finrepVsCorep",
        route: {
            name: "finrepVsCorep"
        }
    }
];

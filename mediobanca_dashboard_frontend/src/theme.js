"use strict";

import Vue from "vue";
import {
    Button,
    Select,
    Input,
    Notification,
    Form,
    FormItem,
    Row,
    Col,
    Icon,
    Menu,
    MenuItem,
    MenuItemGroup,
    Submenu,
    Dialog,
    Avatar,
    Tooltip,
    Card,
    Divider,
    Container,
    Main,
    Header,
    Aside,
    Footer,
    Table,
    TableColumn,
    Pagination,
    Carousel,
    CarouselItem,
    Tabs,
    TabPane,
    Collapse,
    CollapseItem,
    Drawer,
    Cascader,
    CascaderPanel,
    Popover,
    Upload,
    Loading
} from "element-ui";
// import "element-ui/lib/theme-chalk/index.css";
// import VueDataTables from "vue-data-tables";
import {library as Library} from "@fortawesome/fontawesome-svg-core";
import {FontAwesomeIcon} from "@fortawesome/vue-fontawesome";
import {faBars as FaBars} from "@fortawesome/free-solid-svg-icons";
import CollapseTransition from "element-ui/lib/transitions/collapse-transition";

Vue.component(CollapseTransition.name, CollapseTransition);
import "./css/overrides.css";

/**
 * Initialize all the theme stuff
 */
export const Theme = {
    init() {
        Library.add(FaBars);
        Vue.component("font-awesome-icon", FontAwesomeIcon);

        Vue.use(Button);
        Vue.use(Select);
        Vue.use(Input);
        Vue.use(Form);
        Vue.use(FormItem);
        Vue.use(Row);
        Vue.use(Col);
        Vue.use(Icon);
        Vue.use(Menu);
        Vue.use(MenuItem);
        Vue.use(MenuItemGroup);
        Vue.use(Submenu);
        Vue.use(Dialog);
        Vue.use(Avatar);
        Vue.use(Tooltip);
        Vue.use(Card);
        Vue.use(Divider);
        Vue.use(Container);
        Vue.use(Main);
        Vue.use(Header);
        Vue.use(Aside);
        Vue.use(Footer);
        Vue.use(Table);
        Vue.use(TableColumn);
        Vue.use(Pagination);
        Vue.use(Carousel);
        Vue.use(CarouselItem);
        Vue.use(Tabs);
        Vue.use(TabPane);
        Vue.use(Collapse);
        Vue.use(CollapseItem);
        Vue.use(Drawer);
        Vue.use(Cascader);
        Vue.use(CascaderPanel);
        Vue.use(Popover);
        Vue.use(Upload);
        Vue.use(Loading);
        Vue.prototype.$notify = Notification;
    }
};

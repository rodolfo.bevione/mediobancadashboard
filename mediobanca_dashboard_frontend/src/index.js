"use strict";

import Vue from "vue";
import {Store} from "./store.js";
import {Router} from "./router.js";
import Main from "./template/main.vue";
import {i18n} from "./i18n";
import {Theme} from "./theme";
import "regenerator-runtime/runtime";

Theme.init();

Vue.config.productionTip = process.env.NODE_ENV === "development";
Vue.config.devtools = process.env.NODE_ENV === "development";

Vue.prototype.$store = Store;

Vue.prototype.$store.commit("setUserData", {
    token: window.localStorage.getItem("dashboardAuthToken"),
});

Vue.prototype.$notifyInternal = function(notification) {
    // This is to let the store throw notifications (mostly for HTTP errors, but not just for this)
    Vue.prototype.$notify({
        title: i18n.t(notification.title),
        message: i18n.t(notification.message),
        type: notification.type
    });
};

window.onload = () => {
    new Vue({
        router: Router,
        store: Store,
        i18n: i18n,
        render: (h) => h(Main),
    }).$mount(
        (() => {
            const root = document.createElement("div");
            root.id = "mediobanca-dashboard";
            document.body.append(root);
            return root;
        })()
    );
};

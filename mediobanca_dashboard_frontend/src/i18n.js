import It from "./i18n/it";
import VueI18n from "vue-i18n";
import Vue from "vue";

Vue.use(VueI18n);

export const i18n = new Proxy(new VueI18n({
    locale: "it",
    messages: {
        it: It
    }
}), {
    set(object, key, value) {
        if (key === "locale") {
            object.value = value.toLowerCase();
        } else {
            object[key] = value;
        }
        return true;
    }
});

"use strict";

import AxiosClient from "axios";
import Vue from "vue";

const Axios = AxiosClient.create({
    baseURL: process.env.BACKEND_BASE_URL,
    withCredentials: true,
    timeout: Number.parseInt(process.env.AXIOS_DEFAULT_TIMEOUT),
});

/**
 * Stuff we use to manage and eventually suppress request interceptors
 * @author Rodolfo Bevione
 * @class
 * @todo TODO   Move notification logic outside of here, to avoid having excessive Vue bindings in a non-vue file
 */
class Interceptor {
    /**
     * Build a new interceptor
     * @constructor
     * @param {Axios} axiosInstance The axios instance we will work on
     */
    constructor(axiosInstance) {
        this.axiosInstance = axiosInstance;
        this.axiosInstance.interceptors.response.use(
            (...args) => this.requestSuccessManage(...args),
            (...args) => this.requestErrorManage(...args)
        );
        this.interceptors = {
            401: true,
            403: true,
            404: true,
            500: true
        };
        this.stack = [];
    }
    /**
     * Axios will start using the given response interceptor
     * @param   {string}  whatToEnable  Name of the interceptor to enable
     * @return  {Interceptor}           Fluent
     */
    enable(whatToEnable) {
        this.interceptors[whatToEnable] = true;
        return this;
    }
    /**
     * Axios will stop using the given response interceptor
     * @param   {string}  whatToDisable Name of the interceptor to disable
     * @return  {Interceptor}   Fluent
     */
    disable(whatToDisable) {
        this.interceptors[whatToDisable] = false;
        return this;
    }
    /**
     * What to do when we have a succesful response
     * @param   {*} response    Our error response
     * @return  {*}
     */
    requestSuccessManage(response) {
        Vue.prototype.$store.commit("setUserData", {
            token: response.headers.authorization
        });
        return response;
    }
    /**
     * What to do when we have an error response
     * @param   {*} error   Our error response
     * @return  {*} Promise error
     * @todo    TODO    In case of error 500 we need to find a way to pass the operation name around
     */
    requestErrorManage(error) {
        // var result = Promise.reject(error);
        switch (error.response.status) {
        case 401:
            if (this.interceptors["401"]) {
                Vue.prototype.$store.dispatch("logout");
                Vue.prototype.$notifyInternal({
                    title: "shared.error401.title",
                    message: "shared.error401.content",
                    type: "error"
                });
            } else {
                return Promise.reject(error);
            }
            break;
        case 403:
            if (this.interceptors["403"]) {
                Vue.prototype.$notifyInternal({
                    title: "shared.error403.title",
                    message: "shared.error403.content",
                    type: "error"
                });
            } else {
                return Promise.reject(error);
            }
            break;
        case 404:
            if (this.interceptors["404"]) {
                Vue.prototype.$notifyInternal({
                    title: "shared.error404.title",
                    message: "shared.error404.content",
                    type: "error"
                });
            } else {
                return Promise.reject(error);
            }
            break;
        case 500:
            if (this.interceptors["500"]) {
                Vue.prototype.$notifyInternal({
                    title: "shared.error500.title",
                    message: "shared.error500.content",
                    type: "error"
                });
            } else {
                return Promise.reject(error);
            }
            break;
        default:
            return Promise.reject(error);
            break;
        }
        // return result;
    }
    // TODO implement this
    // eslint-disable-next-line require-jsdoc
    stackAdd(something) {
        this.stack;
    }
    // TODO implement this
    // eslint-disable-next-line require-jsdoc
    stackManage() {
    }
}
export {
    Axios,
    Interceptor
};

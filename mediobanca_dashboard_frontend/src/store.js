"use strict";
import Vue from "vue";
import Vuex from "vuex";
import {Axios, Interceptor} from "./common";
import {i18n} from "./i18n";

Vue.use(Vuex);

/**
 * Just declaring what does a user have, for your own personal refernce
 * @class
 */
class User {
    /**
     * @constructor
     */
    constructor() {
        this.username = null;
        this.avatar = null;
        this.id = null;
        this.token = null;
        this.locale = "it";
    }
}

export const Store = new Vuex.Store({
    state: {
        user: new User(),
        ui: {
            sidebar: {
                isCollapsed: true,
                active: "none"
            },
            shortcuts: {
            },
        },
        search: {
            results: null
        },
        network: {
            Axios: Axios,
            Interceptor: new Interceptor(Axios)
        }
    },
    mutations: {
        searchResultsSet(state, searchResults) {
            state.search.results = searchResults;
        },
        sidebarCollapse(state) {
            state.ui.sidebar.isCollapsed = true;
        },
        sidebarExpand(state) {
            state.ui.sidebar.isCollapsed = false;
        },
        sidebarToggle(state) {
            state.ui.sidebar.isCollapsed = !state.ui.sidebar.isCollapsed;
        },
        setUserData(state, newUser) {
            // We can't have a "null" user. User is emptied when a null value is set.
            newUser = newUser === null || typeof newUser === "undefined" ?
                new User() :
                newUser;

            /*
            try {
                newUser.locale = newUser.locale.toLowerCase();
            } catch (ignore) {}
            */
            if (typeof newUser.locale !== "undefined") {
                i18n.locale = newUser.locale;
            }
            // Assign new user data
            Object.assign(state.user, newUser);
            // And store\remove the token in LocalStorage
            if (typeof newUser.token === null) {
                window.localStorage.removeItem("dashboardAuthToken");
            } else if (typeof newUser.token !== "undefined") {
                window.localStorage.setItem("dashboardAuthToken", newUser.token);
            }
        }
    },
    getters: {
        isLogged(state) {
            return state.user.token !== null;
        }
    },
    actions: {
        async generalSearch(store, searchingFor) {
            var url = new URL(`${process.env.BACKEND_BASE_URL}/mockup/search`);
            url.searchParams.append("searchingFor", searchingFor);
            try {
                const result = await Axios.get(
                    url,
                    {
                        headers: {
                            Authorization: store.state.user.token
                        }
                    }
                );
                store.state.search.results = result.data;
            } catch (error) {
                console.log(error);
            }
        },
        async ping(store) {
            try {
                // We ping everytime we open our app.
                // Pinging means making sure ur token is still valid
                // If yes, we receive user data from the server
                store.state.network.Interceptor.disable(401);
                let userPing = await Axios.get(
                    `${process.env.BACKEND_BASE_URL}/user/ping`,
                    {
                        headers: {
                            Authorization: store.state.user.token
                        }
                    }
                );
                store.commit("setUserData", userPing.data);
                store.state.network.Interceptor.enable(401);
            } catch (error) {
                // Ping did go wrong. Force a login
                store.dispatch("logout");
            }
        },
        async disconnect(store) {
            try {
                await Axios.post(
                    `${process.env.BACKEND_BASE_URL}/user/logout`,
                    {
                        disconnect: true
                    }
                );
            } catch (ignore) {}
            store.commit("setUserData", null);
        },
        async logout(store) {
            try {
                await Axios.post(
                    `${process.env.BACKEND_BASE_URL}/user/logout`
                );
            } catch (ignore) {}
            store.commit("setUserData", null);
        },
        async logoutFromEverything(store) {
            await Axios.post(
                `${process.env.BACKEND_BASE_URL}/user/disconnect`
            );
            store.commit("setUserData", null);
        },
        async login(store, {username, password}) {
            // We handle 401 errors our own way (as in: wrong password popup)
            store.state.network.Interceptor.disable(401);
            // If request results in 401, the error will bubble up and be caught (hopefully by ./views/login.vue)
            const result = await Axios.post(
                `${process.env.BACKEND_BASE_URL}/user/login`,
                {
                    username: username,
                    password: password
                }
            );
            store.state.network.Interceptor.enable(401);
            // Store the token
            store.commit("setUserData", result.data);
            // Turn on again default 401 error handling (as in: show login form)
        },
        async fileUpload(store, fileList) {
            const formData = new FormData();
            Array.from(fileList).forEach((file) => {
                formData.append("file", file);
            });
            Axios.post(
                `${process.env.BACKEND_BASE_URL}/upload`,
                formData,
                {
                    headers: {
                        Authorization: store.state.user.token
                    }
                }
            );
        }
    }
});

"use strict";

const Dotenv = require("dotenv");
const Path = require("path");
const express = require("express");
const MAIN_PATH = Path.resolve(__dirname, "./../..");
const EarliestPackageJson = require(`${MAIN_PATH}/package.json`);
const Fs = require("fs");
var app = express();
Dotenv.config({
    path: Path.resolve(
        MAIN_PATH,
        EarliestPackageJson.environment[process.env.NODE_ENV].configuration
    )
});

app.use(express.static(`${MAIN_PATH}\\${EarliestPackageJson.environment[process.env.NODE_ENV].bundle}`));

(() => {
    var content = null;
    Fs.readFile(
        `${MAIN_PATH}\\${EarliestPackageJson.environment[process.env.NODE_ENV].bundle}\\index.html`,
        "utf8",
        (err, data) => {
            if (err) {
                throw err;
            }
            content = data;
        }
    );
    app.get("*", (request, response) => {
        response.set("Content-Type", "text/html");
        response.send(Buffer.from(content));
    });
})();
app.listen(
    4001,
    () => {
        console.log("File server listening on 4001");
    }
);

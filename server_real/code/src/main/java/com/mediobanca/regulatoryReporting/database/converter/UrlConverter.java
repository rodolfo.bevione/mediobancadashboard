package com.mediobanca.regulatoryReporting.database.converter;

import java.net.MalformedURLException;
import java.net.URL;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class UrlConverter implements AttributeConverter<URL, String>
{
    public static final String COLUMN_DEFINITION = "VARCHAR(16) NOT NULL";

    @Override
    public String convertToDatabaseColumn(URL entityValue)
    {
        return entityValue.toString();
    }

    @Override
    public URL convertToEntityAttribute(String databaseValue)
    {
        URL result = null;
        try {
            result = new URL(databaseValue);
        } catch (MalformedURLException e) {}

        return result;
    }
}

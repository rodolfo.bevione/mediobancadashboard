package com.mediobanca.regulatoryReporting.utils;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.util.Properties;

public class ApplicationProperties {
    static public Properties properties;

    static {
        try {
            properties = PropertiesLoaderUtils.loadProperties(new ClassPathResource("/application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

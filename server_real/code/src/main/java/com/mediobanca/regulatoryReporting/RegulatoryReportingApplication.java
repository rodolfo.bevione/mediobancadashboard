package com.mediobanca.regulatoryReporting;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mediobanca.regulatoryReporting.utils.Internationalization;
import org.modelmapper.ModelMapper;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

import java.io.IOException;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
@EntityScan("com.mediobanca.regulatoryReporting.database")
@ComponentScan({
		"com.mediobanca.regulatoryReporting.http",
		"com.mediobanca.regulatoryReporting.database"
})
public class RegulatoryReportingApplication extends SpringBootServletInitializer {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(RegulatoryReportingApplication.class, args);
	}
	@Bean
	public Internationalization translation() throws IOException {
		return new Internationalization();
	}
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
	@Bean
	public Gson gson() {
		return new GsonBuilder()
				.excludeFieldsWithoutExposeAnnotation()
				.create();
	}

	@Bean
	public CommonsMultipartResolver CommonsMultipartResolver() {
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		multipartResolver.setMaxUploadSize(100000);
		return multipartResolver;
	}
	@Bean
	public StandardServletMultipartResolver StandardServletMultipartResolver() {
		return new StandardServletMultipartResolver();
	}
}
package com.mediobanca.regulatoryReporting.database.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * We use this to store Map<String, Object> inside the DB as JSON strings.
 *
 * @author Rodolfo Bevione
 */
@Converter(autoApply = true)
public class JsonConverter implements AttributeConverter<Map<String, Object>, String> {
    public static final String COLUMN_DEFINITION = "TEXT";

    @Override
    public String convertToDatabaseColumn(Map<String, Object> entityValue) {
        ObjectMapper mapper = new ObjectMapper();
        String result = "null";

        try {
            result = mapper.writeValueAsString(entityValue);
        } catch (JsonProcessingException ex) {
            // TODO: LOG
        }

        return result;
    }

    @Override
    public Map<String, Object> convertToEntityAttribute(String databaseValue) {
        ObjectMapper mapper = new ObjectMapper();

        Map<String, Object> result = new HashMap<String, Object>();
        try {
            result = new ObjectMapper().readValue(databaseValue, Map.class);
        } catch (IOException ex) {
            // TODO: LOG
        }
        return result;
    }
}

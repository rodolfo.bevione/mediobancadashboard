package com.mediobanca.regulatoryReporting.database.file;

import com.mediobanca.regulatoryReporting.database.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "files")
public class File extends BaseEntity {
    public enum FileClassification {
        FINREP, COREP, ANACREDIT
    }

    @Column(nullable = false)
    private String fileName;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private FileClassification classifiedAs;

    public File () {
        this("default");
    }
    public File(String name) {
        this.setFileName(name);
    }

    public FileClassification classify() {
        return FileClassification.FINREP;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public FileClassification getClassifiedAs() {
        return classifiedAs;
    }

    public void setClassifiedAs(FileClassification classifiedAs) {
        this.classifiedAs = classifiedAs;
    }
}

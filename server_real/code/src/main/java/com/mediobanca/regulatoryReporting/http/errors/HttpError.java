package com.mediobanca.regulatoryReporting.http.errors;

import com.google.gson.Gson;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public interface HttpError {
    public int getCode();
    default public void write(HttpServletResponse response) throws IOException {
        response.setStatus(this.getCode());
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        out.write(new Gson().toJson(this));
    }
}

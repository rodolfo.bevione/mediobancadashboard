package com.mediobanca.regulatoryReporting.utils;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.mediobanca.regulatoryReporting.config.Locale;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Internationalization {
    private Map<Locale, Properties> internationalization = new HashMap<>();

    public Internationalization() throws IOException {
        this.internationalization.put(
            Locale.IT,
            PropertiesLoaderUtils.loadProperties(new ClassPathResource("/it.properties"))
        );
        this.internationalization.put(
            Locale.EN,
            PropertiesLoaderUtils.loadProperties(new ClassPathResource("/en.properties"))
        );
    }
    public String translate(Locale locale, String message) {
        return this.internationalization.get(locale).getProperty(message, message);
    }
}

package com.mediobanca.regulatoryReporting.http.controller.userController;

import com.mediobanca.regulatoryReporting.config.ErrorCodes;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class UserDtoAskReset {
    @NotNull
    @NotEmpty
    @Email(message = ErrorCodes.UserRegistration.INVALID_EMAIL)
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

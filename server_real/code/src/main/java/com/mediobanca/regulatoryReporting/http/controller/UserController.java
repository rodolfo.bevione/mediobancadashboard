package com.mediobanca.regulatoryReporting.http.controller;

import com.mediobanca.regulatoryReporting.database.user.User;
import com.mediobanca.regulatoryReporting.database.user.UserRepository;
import com.mediobanca.regulatoryReporting.http.controller.userController.UserDto;
import com.mediobanca.regulatoryReporting.database.user.userService.EmailExistsException;
import com.mediobanca.regulatoryReporting.http.controller.userController.UserDtoAskReset;
import com.mediobanca.regulatoryReporting.http.middleware.authentication.JwtLogoutHandler;
import com.mediobanca.regulatoryReporting.http.middleware.authentication.RrUserDetails;
import com.mediobanca.regulatoryReporting.http.middleware.authentication.RrUserDetailsService;
import com.mediobanca.regulatoryReporting.utils.FileStorageService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;

@RestController
public class UserController {
    @Autowired
    private HttpServletRequest req;
    @Autowired
    private RrUserDetailsService userService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private FileStorageService fileStorageService;
    @Autowired
    private ModelMapper modelMapper;

    @RequestMapping(method = RequestMethod.POST,
                value = "/user")
    @ResponseBody
    public void registerUserAccount (@ModelAttribute("user") @Valid UserDto accountDto,
                                     BindingResult result,
                                     WebRequest request,
                                     Errors errors) throws EmailExistsException {
        if (!result.hasErrors()) {
            userService.registerNewUserAccount(accountDto);
        }
    }

    @RequestMapping(method = RequestMethod.PATCH,
        value="/user/{userId")
    public void setAvatar(@RequestParam("file") MultipartFile file, @RequestParam String id, Authentication authentication) throws IOException {
        RrUserDetails user = userService.loadUserById(id);

        if (authentication.getPrincipal().equals(user)) {
           String resultingPath = fileStorageService.fileStore(file);
           user.getUser().setAvatar(resultingPath);
           this.userRepository.save(user.getUser());
        } else {
            throw new NoResultException("Can't find this user");
        }
    }

    @RequestMapping(method = RequestMethod.PUT,
                value = "/user/{userId}")
    @ResponseBody
    public void editUser (@ModelAttribute("user") @Valid UserDto accountDto,
                          BindingResult result,
                          WebRequest request,
                          Errors errors) {

    }
    @RequestMapping(method = RequestMethod.GET,
                    value = "/user/ping")
    public ResponseEntity ping(HttpServletRequest request) {
        return ResponseEntity.ok(
                this.convertToDto(
                        ((RrUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).
                                getUser()
                )
        );
    }

    @RequestMapping(method = RequestMethod.POST,
            value = "/user/send-reset-code")
    public ResponseEntity sendResetCode(UserDtoAskReset askReset) {
        this.userService.sendResetCode(this.userService.loadUserByUsername(askReset.getEmail()).getUser());
        return ResponseEntity.noContent().build();
    }

    private UserDto convertToDto(User user) {
        UserDto userDto = modelMapper.map(user, UserDto.class);
        userDto.setPassword(null);
        userDto.setMatchingPassword(null);
        return userDto;
    }
}
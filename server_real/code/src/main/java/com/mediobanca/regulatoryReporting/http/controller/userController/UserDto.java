package com.mediobanca.regulatoryReporting.http.controller.userController;

import com.mediobanca.regulatoryReporting.config.ErrorCodes;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class UserDto {
    public static final String PASSWORD_VALIDATOR = "([\\w+\\s]+){3}";

    @NotNull
    @NotEmpty
    private String username;

    @NotNull
    @NotEmpty
    @Pattern(
            message = ErrorCodes.UserRegistration.INVALID_PASSWORD,
            regexp = UserDto.PASSWORD_VALIDATOR
    )
    private String password;

    private String matchingPassword;

    @NotNull
    @NotEmpty
    @Email(message = ErrorCodes.UserRegistration.INVALID_EMAIL)
    private String email;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
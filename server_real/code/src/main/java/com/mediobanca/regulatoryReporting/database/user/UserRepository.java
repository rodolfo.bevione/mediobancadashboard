package com.mediobanca.regulatoryReporting.database.user;

import com.mediobanca.regulatoryReporting.database.user.User;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.UUID;


// This talks directly to the DB
@Repository
@Service
@Transactional
public class UserRepository {
    @PersistenceContext
    EntityManager em;

    public User findByEmail(String email) {
        User result = null;
        try {
            result = this.em.createQuery("SELECT u FROM User u JOIN FETCH u.roles WHERE u.email = :email", User.class).
                    setParameter("email", email).
                    getSingleResult();
        } catch(NoResultException ignore) {}
        return result;
    }

    public User findById(String id) {
        return this.em.createQuery("SELECT u FROM User u WHERE u.id = :id", User.class).
                setParameter("id", UUID.fromString(id)).
                getSingleResult();
    }

    // TODO
    // EclipseLink behaves consistently when updating\saving; it performs an upsert
    // Hibernate wants different methods to be called, apparently?
    // Either way, we choose whether we should persist OR merge depending on whether the entity has an ID or not
    // I don't really think that's a good practice, really, we should consider finding a different method to get this done
    public void save (User user) {
        if (null == user.getId()) {
            this.em.persist(user);
        } else {
            this.em.merge(user);
        }
        this.em.flush();
    }
}

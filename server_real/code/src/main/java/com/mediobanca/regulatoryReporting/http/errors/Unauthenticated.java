package com.mediobanca.regulatoryReporting.http.errors;

import javax.servlet.http.HttpServletResponse;

public class Unauthenticated implements HttpError {
    private String message = "You are not authenticated";
    private transient int code = HttpServletResponse.SC_UNAUTHORIZED;

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }
}

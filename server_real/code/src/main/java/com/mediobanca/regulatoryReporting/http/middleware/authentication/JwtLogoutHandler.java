package com.mediobanca.regulatoryReporting.http.middleware.authentication;

import com.google.gson.Gson;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class JwtLogoutHandler implements LogoutSuccessHandler, LogoutHandler {
    @Autowired
    private RrUserDetailsService userService;

    @Autowired
    private Gson gson;

    @Autowired
    private JwtConfig jwtConfig;

    @Override
    //TODO
    // JwtTokenAuthenticationFilter should set the user in SecurityContext, and that's how users should be retrieved
    // For some reason it doesn't run while logging out
    // Because of this, we manually parse the cookie and shit; not a good thing tbh
    // We should find a way to retrieve the user in a more graceful way, really
    public void logout(HttpServletRequest request,
                       HttpServletResponse response,
                       Authentication authentication) {
        Map<String, Object> requestBody = null;
        try {
            requestBody = gson.fromJson(request.getReader(), new TypeToken<Map<String, Object>>() {}.getType());
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(null != requestBody && requestBody.get("disconnect").equals(true)) {
            String token = Arrays.stream(request.getCookies()).
                    filter(cookie -> cookie.getName().equals("authToken")).
                    collect(Collectors.toList()).
                    get(0).
                    getValue();
            this.userService.logout(jwtConfig.decode(token).getSubject());
        }
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest request,
                                HttpServletResponse response,
                                Authentication authentication){
    }
}
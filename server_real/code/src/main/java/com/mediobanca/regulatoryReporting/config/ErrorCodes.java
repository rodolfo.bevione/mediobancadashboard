package com.mediobanca.regulatoryReporting.config;

/**
 * This class contains all the error codes -as in, a bit more descriptive than a simple 400 error- and they explain precisely WHAT went wrong
 * The frontend, whatever will it be, will then interpret these error codes as it pleases
 *
 * @author  Rodolfo Bevione
 */
public class ErrorCodes {
    public static class UserRegistration {
        public static final String DESCRIPTION = "USER_REGISTRATION_FAILED";
        public static final
            String INVALID_EMAIL = "INVALID_EMAIL";
        public static final
            String INVALID_PASSWORD = "INVALID_PASSWORD";
        public static final
            String USER_ALREADY_PRESENT = "USER_ALREADY_PRESENT";
    }
}

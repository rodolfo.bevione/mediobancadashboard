package com.mediobanca.regulatoryReporting.database.user;

import com.google.gson.annotations.Expose;
import com.mediobanca.regulatoryReporting.config.Locale;
import com.mediobanca.regulatoryReporting.database.BaseEntity;
import com.mediobanca.regulatoryReporting.database.role.Role;
import com.mediobanca.regulatoryReporting.database.converter.UrlConverter;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Entity
@Table(name = "users")
public class User extends BaseEntity {
    public static final String DEFAULT_AVATAR = "https://toppng.com/public/uploads/preview/app-icon-set-login-icon-comments-avatar-icon-11553436380yill0nchdm.png";

    @Size(max = 50)
    @Column(name = "username",
            unique = true)
    @Expose
    private String username;

    @Size(max = 60)
    @Column(name = "email",
            unique = true)
    @Expose
    private String email;

    @Size(max = 60)
    @Column(name = "password")
    private String password;

    @Column(name = "avatar", length = 255)
    @Convert(converter = UrlConverter.class)
    @Expose
    private URL avatar;

    @Column(name = "locale",
            columnDefinition = "ENUM('IT', 'EN')")
    @Enumerated(EnumType.STRING)
    @Expose
    private Locale locale;

    @Size(max = 16)
    @Column(name = "temp_reset_code")
    private String tempResetCode;

    @ManyToMany(fetch = FetchType.LAZY,
                cascade = CascadeType.ALL)
    @MapKey(name = "name")
    private Map<String, Role> roles = new ConcurrentHashMap<>();

    // That's a timestamp; if the token was issued BEFORE this nullification, the request is denied
    @Basic(optional = false)
    @Column(name = "token_valid_from",
            columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tokenValidFrom;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Map<String, Role> getRoles() {
        return roles;
    }

    public void setRoles(Map<String, Role> roles) {
        this.roles = roles;
    }

    public Date getTokenValidFrom() {
        return tokenValidFrom;
    }

    public void setTokenValidFrom(Date lastTokenNullified) {
        this.tokenValidFrom = lastTokenNullified;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public URL getAvatar() {
        return avatar;
    }

    public void setAvatar(URL avatar) {
        this.avatar = avatar;
    }

    public void setAvatar(String avatar) throws MalformedURLException {
        this.avatar = new URL(avatar);
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public String getTempResetCode() {
        return this.tempResetCode;
    }

    public void setTempResetCode(String tempResetCode) {
        this.tempResetCode = tempResetCode;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", roles=" + roles +
                ", tokenValidFrom=" + tokenValidFrom +
                '}';
    }



}

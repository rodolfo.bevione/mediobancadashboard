package com.mediobanca.regulatoryReporting.http.middleware.authentication;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Date;


public class JwtConfig {
    @Value("${server.security.jwtType}")
    private String JWT_TYPE;
    @Value("${server.security.jwtKey}")
    private String JWT_KEY;
    @Value("${spring.config.name}")
    private String JWT_ISSUER;
    @Value("${server.security.jwtDuration}")
    private Long JWT_DURATION;
    @Value("${server.security.jwtRenewalStartsAt}")
    private Long JWT_RENEWAL_STARTS_AT;


    public Long getJwtDuration() {
        return this.JWT_DURATION;
    }
    public Long getJwtRenewalStartsAt() {return this.JWT_RENEWAL_STARTS_AT;}
    public void setJwtType(String jwtType) {
        JWT_TYPE = jwtType;
    }

    public void setJwtDuration(String jwtDuration) {
        setJwtDuration(Long.parseLong(jwtDuration));
    }
    public void setJwtDuration(Long jwtDuration) {
        JWT_DURATION = jwtDuration;
    }

    public void setJwtKey(String jwtKey) {
        JWT_KEY = jwtKey;
    }
    public void setJwtIssuer(String jwtIssuer) {
        JWT_ISSUER = jwtIssuer;
    }
    public void setJwtRenewalStartsAt(String jwtRenewalStartsAt) {setJwtRenewalStartsAt(Long.parseLong(jwtRenewalStartsAt));}
    public void setJwtRenewalStartsAt(Long jwtRenewalStartsAt) {JWT_RENEWAL_STARTS_AT = jwtRenewalStartsAt;}

    public Claims decode(String token) {
        return Jwts.parser().
                setSigningKey(JWT_KEY.getBytes()).
                parseClaimsJws(token).
                getBody();
    }
    public boolean shouldRenew(Claims claims) {
        var renewalStartsAt = new Date(claims.getIssuedAt().getTime() + JWT_RENEWAL_STARTS_AT * 1000);
        var now = new Date(System.currentTimeMillis());
        return now.after(renewalStartsAt) && now.before(claims.getExpiration());
    }
    public String getToken(String username) {
        Key key = Keys.hmacShaKeyFor(JWT_KEY.getBytes(StandardCharsets.UTF_8));
        Long now = System.currentTimeMillis();
        JwtBuilder builder = Jwts.builder();
        builder.setSubject(username);
        builder.setIssuedAt(new Date(now));
        builder.setExpiration(new Date(now + JWT_DURATION * 1000));
        builder.setIssuer(JWT_ISSUER);
        builder.signWith(key);
        return builder.compact();
    }

    @Override
    public String toString() {
        return "JwtConfig{" +
                "JWT_TYPE='" + JWT_TYPE + '\'' +
                ", JWT_KEY='" + JWT_KEY + '\'' +
                ", JWT_ISSUER='" + JWT_ISSUER + '\'' +
                ", JWT_DURATION=" + JWT_DURATION +
                ", JWT_RENEWAL_STARTS_AT=" + JWT_RENEWAL_STARTS_AT +
                '}';
    }
}
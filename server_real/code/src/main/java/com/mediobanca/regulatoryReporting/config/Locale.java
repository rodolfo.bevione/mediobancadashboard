package com.mediobanca.regulatoryReporting.config;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum Locale {
    IT, EN;

    // Now, this thing is totally useless because JPA column definitions must be constants known at compile time, and this is not the case
    // But it's my first stream usage and DEAR GOD I LOVE IT
    public static final
        String COLUMN_DEFINITION = String.format(
            "ENUM (%s)",
            String.join(
        ",",
                Stream.of(Locale.values()).
                    map(Locale::name).
                    map((locale) -> String.format("'%s'", locale)).
                    collect(Collectors.toList())
                )
        );
}

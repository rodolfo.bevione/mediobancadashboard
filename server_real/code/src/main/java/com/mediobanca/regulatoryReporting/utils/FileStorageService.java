package com.mediobanca.regulatoryReporting.utils;

import org.apache.commons.io.FilenameUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.*;

@Service
public class FileStorageService {

    private final Path fileStorageLocation;

    public FileStorageService() {
        this.fileStorageLocation = Paths.get(ApplicationProperties.properties.getProperty("server.config.multipartFormDataLocation")).
                toAbsolutePath().
                normalize();

        // TODO log if can't create directory
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ignore) {}
    }

    private boolean isPathAllowed(MultipartFile file) {
        return isPathAllowed(StringUtils.cleanPath(file.getOriginalFilename()));
    }
    private boolean isPathAllowed(String path) {
        return isPathAllowed(fileStorageLocation.resolve(path));
    }
    private boolean isPathAllowed (Path path) {
        return path.toAbsolutePath().startsWith(fileStorageLocation.toAbsolutePath());
    }
    public String fileStore(MultipartFile file) throws InvalidPathException, IOException {
        if (!isPathAllowed(file)) {
            // You gotta search files inside the public folder, buddy, or I'll bounce you
            throw new InvalidPathException(
                file.getOriginalFilename(),
                String.format("\"%s\" is not a valid file name", file.getOriginalFilename())
            );
        }

        String resultingPath = String.format("%s/%s",
                getSubFolder(file),
                file.getOriginalFilename());
        Files.copy(file.getInputStream(),
                fileStorageLocation.resolve(resultingPath),
                StandardCopyOption.REPLACE_EXISTING);
        return resultingPath;
    }

    public Resource fileLoad(String fileName) throws FileNotFoundException {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            if (isPathAllowed(filePath)) {
                // You gotta search files inside the public folder, buddy, or I'll bounce you
                throw new FileNotFoundException(String.format("File \"%s\" not found", fileName));
            }
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new FileNotFoundException(String.format("File \"%s\" not found", fileName));
            }
        } catch (MalformedURLException ex) {
            throw new FileNotFoundException(String.format("File \"%s\" not found", fileName));
        }
    }
    // TODO this should probaby go into some kind of configuration file
    private String getSubFolder(MultipartFile file) {
        String result = "";
        switch(FilenameUtils.getExtension(file.getOriginalFilename()).toLowerCase()) {
            case "png":
            case "svg":
            case "gif":
            case "jpg":
            case "jpeg" :
            case "jfif":
                result = "img";
                break;
            default:
                result = "misc";
                break;
        }
        return result;
    }
}
package com.mediobanca.regulatoryReporting.http.middleware.authentication;

import com.github.jknack.handlebars.Handlebars;
import com.mediobanca.regulatoryReporting.http.controller.userController.UserDtoResetPassword;
import com.mediobanca.regulatoryReporting.utils.Internationalization;
import org.apache.commons.io.IOUtils;
import com.mediobanca.regulatoryReporting.config.Locale;
import com.mediobanca.regulatoryReporting.database.role.RoleRepository;
import com.mediobanca.regulatoryReporting.database.user.User;
import com.mediobanca.regulatoryReporting.http.controller.userController.UserDto;
import com.mediobanca.regulatoryReporting.database.user.UserRepository;
import com.mediobanca.regulatoryReporting.database.user.userService.EmailExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.*;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Service
public class RrUserDetailsService implements UserDetailsService {
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private Validator validator;
    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private Internationalization internationalization;
    @Autowired
    private JavaMailSender javaMailSender;

    @Value("#{'${server.security.defaultRole}'.split(',')}")
    private List<String> defaultUserRoles;


    @Override
    public RrUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return new RrUserDetails(this.userRepository.findByEmail(username));
    }

    public RrUserDetails loadUserById(String id) {
        return new RrUserDetails(this.userRepository.findById(id));
    }
    public User registerNewUserAccount (UserDto userDto) throws EmailExistsException {
        var user = new User();
        if(userRepository.findByEmail(userDto.getEmail()) != null) {
            throw new EmailExistsException();
        } else if (!userDto.getPassword().equals(userDto.getMatchingPassword())) {
            throw new EmailExistsException();
        } else {
            try {
                user.setAvatar(user.DEFAULT_AVATAR);
            } catch (MalformedURLException ignore) {}
            user.setEmail(userDto.getEmail());
            user.setUsername(userDto.getEmail());
            user.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
            user.setLocale(Locale.IT);
            defaultUserRoles.forEach((role) -> {
                user.getRoles().put(role, roleRepository.findByName(role));
            });
            userRepository.save(user);
        }
        return user;
    }

    public void logout(String username) {
        this.logout(this.loadUserByUsername(username).getUser());
    }
    public void logout(RrUserDetails user) {
        this.logout(user.getUser());
    }
    public void logout(User user) {
        user.setTokenValidFrom(new Date(System.currentTimeMillis()));
        this.userRepository.save(user);
    }
    public void sendResetCode(User user) {

        ClassLoader classLoader = getClass().getClassLoader();

        try (InputStream inputStream = classLoader.getResourceAsStream(internationalization.translate(
                user.getLocale(),
                "email.forgottenPassword.bodyPath"))
        ) {
            //TODO must generate this string with some cryptographically secure thing
            user.setTempResetCode("asdqwe");
            var handlebars = new Handlebars();
            var template = handlebars.compileInline(IOUtils.toString(inputStream, StandardCharsets.UTF_8));

            var msg = new SimpleMailMessage();
            msg.setTo(user.getEmail());
            msg.setSubject(internationalization.translate(user.getLocale(), "email.forgottenPassword.subject"));
            msg.setText(template.apply(user));

            javaMailSender.send(msg);
            user.setTempResetCode(bCryptPasswordEncoder.encode(user.getTempResetCode()));
            this.userRepository.save(user);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void resetPassword(UserDtoResetPassword userDto) throws Exception {
        var user = userRepository.findByEmail(userDto.getEmail());
        if(bCryptPasswordEncoder.matches(userDto.getTempResetCode(), user.getTempResetCode())) {
            if(userDto.getPassword().equals(userDto.getMatchingPassword())) {
                user.setTempResetCode(null);
                user.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
                userRepository.save(user);
            }
            else {
                throw new Exception ("Password non corrispondenti");
            }
        } else {
            throw new Exception("Wrong reset code");
        }
    }

    public Set<ConstraintViolation<User>> userValidate(User user) {
        return this.validator.validate(user);
    }
}

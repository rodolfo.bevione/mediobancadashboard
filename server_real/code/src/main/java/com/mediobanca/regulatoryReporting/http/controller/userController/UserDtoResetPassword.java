package com.mediobanca.regulatoryReporting.http.controller.userController;

import com.mediobanca.regulatoryReporting.config.ErrorCodes;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Objects;

public class UserDtoResetPassword {
    @NotNull
    @NotEmpty
    @Email(message = ErrorCodes.UserRegistration.INVALID_EMAIL)
    private String email;

    @NotNull
    @NotEmpty
    @Pattern(
            message = ErrorCodes.UserRegistration.INVALID_PASSWORD,
            regexp = UserDto.PASSWORD_VALIDATOR
    )
    private String password;

    private String matchingPassword;
    private String tempResetCode;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    public String getTempResetCode() {
        return tempResetCode;
    }

    public void setTempResetCode(String tempResetCode) {
        this.tempResetCode = tempResetCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDtoResetPassword that = (UserDtoResetPassword) o;
        return Objects.equals(email, that.email) &&
                Objects.equals(password, that.password) &&
                Objects.equals(matchingPassword, that.matchingPassword) &&
                Objects.equals(tempResetCode, that.tempResetCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, password, matchingPassword, tempResetCode);
    }

    @Override
    public String toString() {
        return "UserDtoResetPassword{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", matchingPassword='" + matchingPassword + '\'' +
                ", tempResetCode='" + tempResetCode + '\'' +
                '}';
    }
}

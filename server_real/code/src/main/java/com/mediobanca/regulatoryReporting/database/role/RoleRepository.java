package com.mediobanca.regulatoryReporting.database.role;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
@Service
@Transactional
public class RoleRepository {
    @PersistenceContext
    EntityManager em;
    public Role findByName(String name) {
        return em.createQuery("SELECT r FROM Role r WHERE r.name = :name", Role.class).
                setParameter("name", name).
                getSingleResult();
    }
    public void save(Role role) {
        this.em.persist(role);
    }
}

package com.mediobanca.regulatoryReporting.http.middleware.authentication;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

// When logging with username and password we run THIS to get a token
public class JwtUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private AuthenticationManager authManager;
    @Autowired
    private JwtConfig jwtConfig;

    public JwtUsernamePasswordAuthenticationFilter(AuthenticationManager authManager) {
        this.authManager = authManager;
        this.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/user/login", "POST"));
    }

    @Override
    @Autowired
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        super.setAuthenticationManager(authenticationManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException {
        Authentication result = null;
        if (!request.getMethod().equals("POST")) {
            throw new AuthenticationServiceException(
                    "Authentication method not supported: " + request.getMethod());
        }

        try {
            Map<String, Object> credentials = new Gson().fromJson(
                    request.getReader(),
                    new TypeToken<HashMap<String, Object>>() {}.getType()
            );
            var username = credentials.get("username").toString().trim().toLowerCase();
            var password = credentials.get("password").toString();
            var authRequest = new UsernamePasswordAuthenticationToken(username, password);

            // Allow subclasses to set the "details" property
            setDetails(request, authRequest);
            result = this.authManager.authenticate(authRequest);

        } catch (IOException e) {}
        return result;
    }

    // Upon successful authentication, generate a token.
    // The 'auth' passed to successfulAuthentication() is the current authenticated user.
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                            Authentication auth) {
        response.setStatus(200);
        try {
            response.getWriter().write(
                new GsonBuilder()
                        .excludeFieldsWithoutExposeAnnotation()
                        .create().toJson(
                        ((RrUserDetails) auth.getPrincipal()).getUser()
                )

            );
        } catch(IOException ignore) {}
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        String token = jwtConfig.getToken(auth.getName());
        Cookie tokenContainer = new Cookie("authToken", token);
        tokenContainer.setPath("/");
        tokenContainer.setMaxAge(Math.toIntExact(jwtConfig.getJwtDuration()));
        tokenContainer.setHttpOnly(true);
        response.addCookie(tokenContainer);
    }
}
package com.mediobanca.regulatoryReporting.database.converter;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.TimeZone;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;


@Converter(autoApply = true)
/**
 * We use this to store DateTime as Integers, which make things easier for some statistics-related queries
 *
 */
public class LocalDateTimeConverter implements AttributeConverter<LocalDateTime, Long> {
    public static final String COLUMN_DEFINITION = "TIMESTAMP";
    // public static final String COLUMN_DEFINITION = "INT(11)";

    @Override
    public Long convertToDatabaseColumn(LocalDateTime entityValue) {
        long milliseconds = entityValue.atZone(ZoneId.systemDefault()).
                toInstant().
                toEpochMilli();
        // Unix timestamps work in SECONDS, but java uses MILLISECONDS
        // Since we like compatibility, we will fix that
        return  (Long) (milliseconds / 1000);
    }

    @Override
    public LocalDateTime convertToEntityAttribute(Long databaseValue) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(databaseValue * 1000),
                TimeZone.getDefault().toZoneId());
    }
}

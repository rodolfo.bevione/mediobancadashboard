package com.mediobanca.regulatoryReporting.http.controller;

import com.mediobanca.regulatoryReporting.database.role.Role;
import com.mediobanca.regulatoryReporting.database.role.RoleRepository;
import com.mediobanca.regulatoryReporting.database.user.User;
import com.mediobanca.regulatoryReporting.http.controller.userController.UserDto;
import com.mediobanca.regulatoryReporting.database.user.UserRepository;
import com.mediobanca.regulatoryReporting.database.user.userService.EmailExistsException;
import com.mediobanca.regulatoryReporting.http.middleware.authentication.RrUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

@RestController
//TODO this thing MUST be removed
public class DebugController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RrUserDetailsService userService;

    @Autowired
    private RoleRepository roleRepository;

    @RequestMapping(method = RequestMethod.GET,
                    value = "/debug/index")
    public Map<String, String> index() {
        HashMap<String, String> result = new HashMap<>();
        result.put("content", "Greetings from Spring Boot!");
        return result;
    }
    @RequestMapping(method = RequestMethod.GET,
            value = "/debug/populate")
    public User populate() throws MalformedURLException, EmailExistsException {
        var adminRole = new Role();
        var teamLeaderRole = new Role();
        var userRole = new Role();
        var guestRole = new Role();

        adminRole.setName("ADMIN");
        teamLeaderRole.setName("TEAM_LEADER");
        userRole.setName("USER");
        guestRole.setName("GUEST");

        this.roleRepository.save(adminRole);
        this.roleRepository.save(teamLeaderRole);
        this.roleRepository.save(userRole);
        this.roleRepository.save(guestRole);

        var userDto = new UserDto();
        userDto.setUsername("test");
        userDto.setEmail("test@test.test");
        userDto.setPassword("test");
        userDto.setMatchingPassword("test");

        return this.userService.registerNewUserAccount(userDto);
    }
}
package com.mediobanca.regulatoryReporting.http.middleware.authentication;

import com.mediobanca.regulatoryReporting.http.errors.Unauthenticated;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;

public class JwtTokenAuthenticationFilter extends BasicAuthenticationFilter {
    @Autowired
    private AuthenticationManager authManager;
    @Autowired
    private JwtConfig jwtConfig;
    @Autowired
    RrUserDetailsService userDetailsService;

    public JwtTokenAuthenticationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        try {
            // Very long way of saying "get cookie by name"
            String token = Arrays.stream(request.getCookies()).
                    filter(cookie -> cookie.getName().equals("authToken")).
                    collect(Collectors.toList()).
                    get(0).
                    getValue();
            // Parse the token, get the username

            Claims claims = jwtConfig.decode(token);
            String username = claims.getSubject();
            Date expiresAt = claims.getExpiration();
            Date issuedAt = claims.getIssuedAt();
            var user = this.userDetailsService.loadUserByUsername(username);
            var now = new Date();
            if (username != null && now.before(expiresAt) && issuedAt.after(user.getUser().getTokenValidFrom())) {
                // Authenticate
                SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(
                        user,
                        null,
                        user.getAuthorities()
                ));
                System.out.println("setto " + user.getUser().getEmail());
                if (jwtConfig.shouldRenew(claims)) {
                    String newToken = jwtConfig.getToken(username);
                    Cookie tokenContainer = new Cookie("authToken", newToken);
                    tokenContainer.setPath("/");
                    tokenContainer.setMaxAge(Math.toIntExact(jwtConfig.getJwtDuration()));
                    tokenContainer.setHttpOnly(true);
                    response.addCookie(tokenContainer);
                }
            } else {
                SecurityContextHolder.clearContext();
            }
        } catch (Exception e) {
            SecurityContextHolder.clearContext();
            new Unauthenticated().write(response);
            return;
        }
        // go to the next filter in the filter chain
        chain.doFilter(request, response);
    }
}

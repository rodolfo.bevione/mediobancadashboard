package com.mediobanca.regulatoryReporting.database.validation;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.List;

@ResponseStatus(value = HttpStatus.BAD_REQUEST,
        reason="To show an example of a custom message")
public class ValidationError extends RuntimeException {
    private String description;
    private List<String> errors = new ArrayList<String>();

    public ValidationError(String description) {
        this.setDescription(description);
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}

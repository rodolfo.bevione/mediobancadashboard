package com.mediobanca.regulatoryReporting.database.converter;

import java.nio.ByteBuffer;
import java.util.UUID;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = false)
public class UuidConverter implements AttributeConverter<UUID, String>
{
    public static final String COLUMN_DEFINITION = "VARCHAR(16)";

    @Override
    public String convertToDatabaseColumn(UUID entityValue) {
        System.out.println("chiamto");
        return entityValue.toString();
    }

    @Override
    public UUID convertToEntityAttribute(String databaseValue) {
        System.out.println("chasdadas");
        return UUID.fromString(databaseValue);
    }
}

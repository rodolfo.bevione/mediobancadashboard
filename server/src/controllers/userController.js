"use strict";

const Controller = require("../controller");
const UnauthenticatedError = require("../errors/unauthenticatedError");
const AuthenticationFilter = require("./../requestFilter/authenticationFilter");

/**
 * Controller to manage a user
 * @author Rodolfo Bevione
 * @class
 */
class UserController extends Controller {
    /**
     * Get a placeholder user
     * @return  {Object}    Fake user
     */
    extractUserData() {
        var userData = {
            id: 1,
            username: "Rodolfo",
            avatar: "https://www.audubon.org/sites/default/files/styles/hero_cover_bird_page/public/h_a1_4767_1_common_raven_andrew_lunt_kk_adult.jpg",
            locale: "it"
        };
        return userData;
    }
    /**
     * Ping
     */
    async ping() {
        var userData = this.extractUserData();
        var token = AuthenticationFilter.tokenEncode(userData);
        AuthenticationFilter.tokenSet(this.response, token);
        this.response.json(userData);
    }

    /**
     * Receive a token to log in with the given user
     */
    login() {
        if (this.request.body.username === "Rod" && this.request.body.password === "Rod") {
            var userData = this.extractUserData();
            var token = AuthenticationFilter.tokenEncode(userData);
            AuthenticationFilter.tokenSet(this.response, token);
            this.response.
                status(200).
                json(userData);
        } else {
            throw new UnauthenticatedError("Unauthorized", "Login failed: invalid credentials provided");
        }
    }
}

module.exports = UserController;

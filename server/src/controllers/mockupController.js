"use strict";

const Controller = require("../controller");

/**
 * Controller to get mockup data
 * @author Rodolfo Bevione
 * @class
 */
class MockupController extends Controller {
    // eslint-disable-next-line require-jsdoc
    generalSearch() {
        this.response.status(200).send({
            users: [{
                name: "Rod",
                email: "rod@rod.rod"
            }],
            files: [{
                name: "rod",
                extension: "xls",
                fullName: "rod.xlsx",
                size: 14,
                lastModified: 123456789
            }]
        });
    }
}

module.exports = MockupController;

"use strict";

// eslint-disable-next-line no-unused-vars
const Application = require("express").application;
const AuthenticationFilter = require("./requestFilter/authenticationFilter");
const UserController = require("./controllers/userController");
const MockupController = require("./controllers/mockupController");

/**
 * Register routes for your application
 * @param   {Application}   app Where should I register your routes
 */
function register(app) {
    app.get(
        "/user/ping",
        AuthenticationFilter.filter,
        (request, response) => UserController.run("ping", request, response)
    );
    app.post(
        "/user/login",
        (request, response) => UserController.run("login", request, response)
    );
    app.get(
        "/mockup/search",
        AuthenticationFilter.filter,
        (request, response) => MockupController.run("generalSearch", request, response)
    );
}

module.exports = register;

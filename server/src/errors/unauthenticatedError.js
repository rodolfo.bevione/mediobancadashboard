"use strict";

const HttpError = require("./httpError");

/**
 * A custom 401 error
 * @author Rodolfo Bevione
 * @class
 * @extends HttpError
 */
class UnauthenticatedError extends HttpError {
    /**
     * Instantiates a 403 server error
     * @constructor
     * @param   {string}    title   Title of this error
     * @param   {message}   message Extensive description of this error
     */
    constructor(title, message) {
        super(
            typeof title === "undefined" ? UnauthenticatedError.DEFAULT_TITLE : title,
            typeof message === "undefined" ? UnauthenticatedError.DEFAULT_MESSAGE : message,
            UnauthenticatedError.CODE
        );
    }

    /**
     * Unauthenticade error code
     * @override
     * @static
     * @return {number} Error code
     */
    static get CODE() {
        return 401;
    }

    /**
     * Unauthenticated error title
     * @override
     * @static
     * @return  {string}    Default title for this error
     */
    static get DEFAULT_TITLE() {
        return "Unauthenticated";
    }

    /**
     * Unauthenticated error message
     * @override
     * @static
     * @return  {string}    Default message for this error
     */
    static get DEFAULT_MESSAGE() {
        return "Login required to use this endpoint";
    }
}

module.exports = UnauthenticatedError;

"use strict";

const HttpError = require("./httpError");

/**
 * A custom 404 error
 * @author Rodolfo Bevione
 * @class
 * @extends HttpError
 */
class NotFoundError extends HttpError {
    /**
     * Instantiates a 404 server error
     * @constructor
     * @param   {string}    title   Title of this error
     * @param   {message}   message Extensive description of this error
     */
    constructor(title, message) {
        super(
            typeof title === "undefined" ? NotFoundError.DEFAULT_TITLE : title,
            typeof message === "undefined" ? NotFoundError.DEFAULT_MESSAGE : message,
            NotFoundError.CODE
        );
    }

    /**
     * Not found error code
     * @override
     * @static
     * @return {number} Error code
     */
    static get CODE() {
        return 404;
    }

    /**
     * Not found default error title
     * @override
     * @static
     * @return  {string}    Default title for this error
     */
    static get DEFAULT_TITLE() {
        return "Not found";
    }

    /**
     * Not found error message
     * @override
     * @static
     * @return  {string}    Default message for this error
     */
    static get DEFAULT_MESSAGE() {
        return "No such resource has been found";
    }
}

module.exports = NotFoundError;

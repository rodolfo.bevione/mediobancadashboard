"use strict";

const HttpResponse = require("express").response;

/**
 * A standard HTTP error
 * @abstract
 * @author  Rodolfo Bevione
 * @class
 * @extends Error
 */
class HttpError extends Error {
    /**
     * Creates a new HttpError object
     * @constructor
     * @param {string} title     Title returned to the user
     * @param {string} message   Message returned to the user
     * @param {number} code      Error code returned to the user
     */
    constructor(title, message, code) {
        super(message);
        // Just making sure you don't instantiate HttpError directly
        if (this.constructor === HttpError) {
            throw new Error(`Class ${this.constructor.name} is abstract and cannot be instantiated`);
        }
        this.title = title;
        this.message = message;
        this.code = code;
        // Response headers are stored as key-value pairs
        this.headers = {};
        this.stack = null;
    }

    /**
     * Instantiate a valid HttpError object from a stock error
     * @param   {Error}     vanillaError    From this we build an instance of HttpError
     * @return {HttpError}                  The custom HttpError the controller class can use
     */
    static fromError(vanillaError) {
        // Any uncaught error will default into a 500 error
        // requiring InternalServerError here to avoid circular references
        const InternalServerError = require("./internalServerError");

        return new InternalServerError();
    }

    /**
     * Return the error code for this class
     * @abstract
     * @static
     * @return  {number}    Code for the error to return
     */
    static get CODE() {
        // This method is abstract, I'm making sure you override it
        throw new Error(`Class ${this.constructor.name} extends HttpError but does not override "static get CODE()"`);
    }

    /**
     * Return the default title for this class
     * @abstract
     * @static
     * @return  {string}    Default title for this error class
     */
    static get DEFAULT_TITLE() {
        throw new Error(`Class ${this.constructor.name} extends HttpError but does not override "static get DEFAULT_TITLE()"`);
    }

    /**
     * Return the default message for this class
     * @abstract
     * @static
     * @return  {string}    Default message for this error class
     */
    static get DEFAULT_MESSAGE() {
        throw new Error(`Class ${this.constructor.name} extends HttpError but does not override "static get DEFAULT_MESSAGE()"`);
    }
}

module.exports = HttpError;

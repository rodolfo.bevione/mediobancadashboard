"use strict";

const HttpError = require("./httpError");

/**
 * A custom 400 error
 * @author Rodolfo Bevione
 * @class
 * @extends HttpError
 */
class BadRequest extends HttpError {
    /**
     * Instantiates a 400 server error
     * @constructor
     * @param   {string}    title   Title of this error
     * @param   {message}   message Extensive description of this error
     */
    constructor(title, message) {
        super(
            typeof title === "undefined" ? BadRequest.DEFAULT_TITLE : title,
            typeof message === "undefined" ? BadRequest.DEFAULT_MESSAGE : message,
            BadRequest.CODE
        );
    }

    /**
     * BadRequest error code
     * @override
     * @static
     * @return {number} Error code
     */
    static get CODE() {
        return 400;
    }

    /**
     * BadRequest default error title
     * @override
     * @static
     * @return  {string}    Default title for this error
     */
    static get DEFAULT_TITLE() {
        return "Bad request";
    }

    /**
     * BadRequest error message
     * @override
     * @static
     * @return  {string}    Default message for this error
     */
    static get DEFAULT_MESSAGE() {
        return "Your request is not following a valid syntax; please refer to the documentation for the correct use of this endpoint";
    }
}

module.exports = BadRequest;

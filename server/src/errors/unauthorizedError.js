"use strict";

const HttpError = require("./httpError");

/**
 * A custom 403 error
 * @author Rodolfo Bevione
 * @class
 * @extends HttpError
 */
class UnauthorizedError extends HttpError {
    /**
     * Instantiates a 403 server error
     * @constructor
     * @param   {string}    title   Title of this error
     * @param   {message}   message Extensive description of this error
     */
    constructor(title, message) {
        super(
            typeof title === "undefined" ? UnauthorizedError.DEFAULT_TITLE : title,
            typeof message === "undefined" ? UnauthorizedError.DEFAULT_MESSAGE : message,
            UnauthorizedError.CODE
        );
    }

    /**
     * Unauthorized error code
     * @override
     * @static
     * @return {number} Error code
     */
    static get CODE() {
        return 403;
    }

    /**
     * UnauthorizedError error title
     * @override
     * @static
     * @return  {string}    Default title for this error
     */
    static get DEFAULT_TITLE() {
        return "Unauthorized";
    }

    /**
     * UnauthorizedError error message
     * @override
     * @static
     * @return  {string}    Default message for this error
     */
    static get DEFAULT_MESSAGE() {
        return "You are not allowed to access this resource";
    }
}

module.exports = UnauthorizedError;

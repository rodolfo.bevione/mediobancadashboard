"use strict";

const HttpError = require("./httpError");

/**
 * A custom 500 error
 * @author Rodolfo Bevione
 * @class
 * @extends HttpError
 */
class InternalServerError extends HttpError {
    /**
     * Instantiates a 404 server error
     * @constructor
     * @param   {string}    title   Title of this error
     * @param   {message}   message Extensive description of this error
     */
    constructor(title, message) {
        super(
            typeof title === "undefined" ? InternalServerError.DEFAULT_TITLE : title,
            typeof message === "undefined" ? InternalServerError.DEFAULT_MESSAGE : message,
            InternalServerError.CODE
        );
    }

    /**
     * Not found error code
     * @override
     * @static
     * @return {number} Error code
     */
    static get CODE() {
        return 500;
    }

    /**
     * Internal server error default title
     * @override
     * @static
     * @return  {string}    Default title for this error
     */
    static get DEFAULT_TITLE() {
        return "Internal server error";
    }

    /**
     * Internal server error default message
     * @override
     * @static
     * @return  {string}    Default message for this error
     */
    static get DEFAULT_MESSAGE() {
        return "We are experiencing technical difficulties. Our IT team has been warned.";
    }
}

module.exports = InternalServerError;

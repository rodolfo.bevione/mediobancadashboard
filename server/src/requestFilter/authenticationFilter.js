"use strict";

const Jwt = require("jwt-simple");

// eslint-disable-next-line no-unused-vars
const HttpRequest = require("express").request;
// eslint-disable-next-line no-unused-vars
const HttpResponse = require("express").response;
const Controller = require("./../controller");
const UnauthenticatedError = require("./../errors/unauthenticatedError");
const BadRequestError = require("./../errors/unauthenticatedError");

/**
 * Filters out unauthenticated requests
 * @abstract
 * @class
 * @author  Rodolfo Bevione
 */
class AuthenticationManager {
/**
     * Filters out unauthenticated users
     *
     * @static
     * @param   {HttpRequest}   request     Http request object
     * @param   {HttpResponse}  response    Http response object.
     *                                      Takes the token from the cookies first, then from the headers
     * @param   {Function}      next        Callback
     * @return  {void}
     */
    static filter(request, response, next) {
        // Take the token
        const token = AuthenticationManager.tokenGet(request);
        // If no token is found, scream
        if (!token) {
            Controller.manageError(
                new UnauthenticatedError(),
                response
            );
        } else {
            try {
                // Decode the token; if it fails, throw an error
                const payload = AuthenticationManager.tokenDecode(token);
                // Check token expiration date; if it expired, throw an error
                if (payload.expiresOn < Date.now()) {
                    Controller.manageError(
                        new UnauthenticatedError(),
                        response
                    );
                } else {
                    const newToken = AuthenticationManager.tokenEncode(payload);
                    AuthenticationManager.tokenSet(response, newToken);
                    next();
                }
            } catch (ex) {
                console.log(ex);
                // Decoding the token threw an error. It was a bad token, bounce the request
                Controller.manageError(
                    new BadRequestError(BadRequestError.DEFAULT_TITLE, "No valid authentication token provided"),
                    response
                );
            }
        }
    };
    /**
     * Store the token inside this response
     * @private
     * @static
     * @param   {HttpResponse}  response    Request containing this token
     * @param   {string}        token       The token content
     * @return  {void}
     */
    static tokenSet(response, token) {
        response.header(
            "Authorization",
            token
        );
        response.header(
            "Access-Control-Expose-Headers",
            "Authorization"
        );
    }
    /**
     * Extract a token from this request
     * @private
     * @param   {HttpRequest}   request     Extract the token from here
     * @return  {string}                    Your token
     */
    static tokenGet(request) {
        const token = typeof request.cookies.authToken !== "undefined" ?
            request.cookies.authToken : typeof request.headers.authorization !== "undefined" ?
                request.headers.authorization :
                false;
        return token;
    }
    /**
     * Get an authenticated token from a payload
     * @param   {*}             payload Anything you may possibly want to authenticate
     * @return  {Promise<string>}       Your token
     * @private
     */
    static tokenEncode(payload) {
        // Deep clone the object
        var workOnThis = JSON.parse(JSON.stringify(payload));
        workOnThis.issuedAt= Date.now();
        workOnThis.expiresOn = workOnThis.issuedAt + (Number.parseInt(process.env.USER_TOKEN_VALIDITY) * 1000);
        return `Bearer ${Jwt.encode(workOnThis, process.env.JWT_KEY)}`;
    }

    /**
     * Decode a token
     * @param   {string}    token   The token to decode
     * @return  {Promise<Object>}   The decoded payload
     * @private
     */
    static tokenDecode(token) {
        return Jwt.decode(
            /Bearer (?<token>[a-zA-Z0-9\-\_\.\+]+={0,2})/.exec(token).groups.token,
            process.env.JWT_KEY
        );
    }

    /**
     * Just making sure you don't instance this thing
     * @abstract
     * @constructor
     */
    constructor() {
        throw new Error(`Class ${this.constructor.name} is abstract and cannot be instantiated`);
    }
}


module.exports = AuthenticationManager;

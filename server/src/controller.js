"use strict";

const HttpError = require("./errors/httpError");
const InternalServerError = require("./errors/internalServerError");

// eslint-disable-next-line no-unused-vars
const HttpRequest = require("express").request;
// eslint-disable-next-line no-unused-vars
const HttpResponse = require("express").response;

/**
 * Controller default class. Only some small convention stuff
 * @abstract
 * @author  Rodolfo Bevione
 * @class
 */
class Controller {
    /**
     * Constructor, requires standard Express request and response
     * @constructor
     * @param   {HttpRequest}   request     Express standard request object
     * @param   {HttpResponse}  response    Express standard response object
     */
    constructor(request, response) {
        if (this.constructor === Controller) {
            throw new Error(`Class ${this.constructor.name} is abstract and cannot be instantiated`);
        }
        this.request = request;
        this.response = response;
    }

    /**
     * Instantiate a controller and run the given method
     * @async
     * @param   {string}        method      Name of the method to run
     * @param   {HttpRequest}   request     Http request object
     * @param   {HttpResponse}  response    Http response object
     * @static
     */
    static async run(method, request, response) {
        var instance = new this(request, response);
        try {
            await instance[method]();
        } catch (error) {
            instance.manageError(error);
        }
    }

    /**
     * Sends the given error to the user through the given response object.
     * Additionally logs data if need be
     *
     * @param {Error}           error       The error to manage.
     *                                      If not an instance of HttpError,
     *                                      an instance of ./errors/internalServerError is used
     * @param {HttpResponse}    response    The error will be sent through this response object
     */
    static manageError(error, response) {
        /** @type {HttpError} */
        var workOnThisError = null;
        if (error instanceof HttpError) {
            workOnThisError = error;
        } else {
            workOnThisError = new InternalServerError();
        }

        if (error instanceof InternalServerError) {
            console.log(error);
        }

        // Set response status code
        response.status(workOnThisError.constructor.CODE);
        // Set response headers
        try {
            Reflect.ownKeys(workOnThisError.headers).forEach((header) => {
                response.setHeader(
                    header,
                    workOnThisError.headers[header]
                );
            });
        } catch (ignore) {

        }

        // Set response body and send
        response.send({
            title: workOnThisError.title,
            message: workOnThisError.message
        });
    }
    /**
     * Controller logic to manage an error
     *
     * @param {Error}           error       The error to manage
     */
    manageError(error) {
        Controller.manageError(error, this.response);
    }
}

module.exports = Controller;

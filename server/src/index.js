"use strict";

const Package = require("./../package.json");
const Dotenv = require("Dotenv");
const Path = require("path");
const express = require("express");
const router = require("./router");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const Error403 = require("./errors/unauthorizedError");

Dotenv.config({
    path: Path.resolve(
        __dirname,
        "./..",
        Package.environment[process.env.NODE_ENV].configuration
    )
});

console.log(Path.resolve(
    __dirname,
    "./..",
    Package.environment[process.env.NODE_ENV].configuration
));

var app = express();

app.use(express.json());
app.use(cookieParser());
app.use(express.urlencoded({
    extended: true
}));

// CORS configuration

var whitelist = new Set(process.env.CORS_ORIGIN_ALLOWED.split(","));

console.log(process.env.CORS_ORIGIN_ALLOWED.split(","));
app.all(
    "*",
    cors({
        origin(origin, callback) {
            if (whitelist.has(origin)) {
                callback(null, true);
            } else {
                console.log("bruh");
                callback(null);
            }
        },
        credentials: true
    })
);

router(app);

app.listen(
    Number.parseInt(process.env.SERVER_PORT),
    () => {
        console.log(`Example app listening on port ${process.env.SERVER_PORT}`);
    }
);
